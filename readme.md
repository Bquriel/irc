1) Run the makefile (make all)
2) Start the server in one shell (./server)
3) Start client(s) in their own shells (./client)
4) As a client, type /join to start chatting!
5) Additional help will be listed, or re-listed with command /help