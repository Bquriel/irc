CC = gcc
CFLAGS = -std=c11 -Wall

all:
	make client
	make server
	make clean
	
client: client.o
	$(CC) $(CFLAGS) client.c -o client

server: server.o
	$(CC) $(CFLAGS) server.c -o server

clean:
	@if [ `find|grep ".o" |wc -l` -gt 0 ] ; then rm *.o ; fi