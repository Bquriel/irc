#define _POSIX_C_SOURCE 200112L
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <signal.h>

#define TCPPORT "9001"
#define UDPPORT "9002"
#define	BUFFER_SIZE 1024
#define UNAME_SIZE 15

struct UDPconnection{
    struct sockaddr *serverAddress;
    socklen_t addrlen;
    int socket;
}; typedef struct UDPconnection UDPConnection;


enum messageType{
    CHAT_JOIN,
    CHAT_OK,
    CHAT_MSG,
    CHAT_NAME,
    CHAT_QUIT,
    CHAT_ERROR
}; typedef enum messageType MessageType;

enum errorType{
    ERROR_NO_USER,
    ERROR_CHAT_FULL,
    ERROR_ALREADY_JOINED,
    ERROR_SERVER_SHUTDOWN
}; typedef enum errorType ErrorType;


struct userState{
    int isInChat;
    char name[ 15 ];
}; typedef struct userState UserState;

UDPConnection *connectUDP( char *ip );
int connectTCP();
int receiveChat( int sockfd, char *buffer, int n );
int sendMessage( int TCPconnection, UDPConnection *UDPconnection, char *buffer, size_t size );
void receiveChatData( UDPConnection *UDPconnection, char *buffer );
void sighandler(int signal);
void printHelp();
void initSigHandler();