#include "client.h"

UserState userState;
UDPConnection *UDPconnection;


/*Pokes the server and saves its UDP information
    ip = ip address of targeted server
Returns pointer to server UDP data*/
UDPConnection *connectUDP( char *ip ){
    int n;
    UDPConnection *connection = malloc( sizeof( UDPConnection ));
    struct sockaddr_storage serverAddr;
    connection->addrlen = sizeof( serverAddr );
    connection->serverAddress = (struct sockaddr*) &serverAddr;

    uint16_t code = htons( CHAT_JOIN );
    char buffer[ 5 ];
    struct addrinfo hints, *res;
    memset( buffer, 0, sizeof( buffer ));
    memset( &hints, 0, sizeof( hints ));

    //COPY VARIABLES TO BUFFER - 2 WAYS
    memcpy( buffer, &code, sizeof( code ));
    //*(uint16_t*)buffer = htons( 10 );
    //*********************************

    hints.ai_family     = AF_UNSPEC;
    hints.ai_socktype   = SOCK_DGRAM;
    hints.ai_protocol   = IPPROTO_UDP;


    if(( n = getaddrinfo( ip, UDPPORT, &hints, &res )) != 0 ){
        fprintf( stderr, "getaddrinfo: %s\n", gai_strerror( n ));
        exit( -1 );
    }

    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 400000;

	struct addrinfo *iter;
	for( iter = res; iter != NULL; iter = iter->ai_next ){
		if(( connection->socket = socket( iter->ai_family, iter->ai_socktype, iter->ai_protocol )) < 0){
			continue;
		}
		if( setsockopt( connection->socket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof( tv )) < 0) {
            perror("Error setsockopt");
        }
        if(( n = sendto( connection->socket, buffer, sizeof( buffer ), 0, iter->ai_addr, iter->ai_addrlen)) < 0 ){
            continue;
        }

        connection->serverAddress = iter->ai_addr;
        connection->addrlen = iter->ai_addrlen;
		break;
	}

	if( n == 0 || n == EAGAIN || n == EWOULDBLOCK ){
	    perror( "Error in receiving UDP" );
        exit( -2 );
	}

    //freeaddrinfo( res );          CLEARS UDP ADDRESS - MEMCPY ETC SHOULD BE USED ON IT BEFORE.

    return connection;
}

/*Creates tcp connection to server
ip is hardcoded here to localhost
Returns file descriptor of TCP connection*/
int connectTCP(){
    int n, sockfd;
    struct addrinfo hints, *res;
    memset( &hints, 0, sizeof( hints ));

    hints.ai_family     = AF_UNSPEC;
    hints.ai_socktype   = SOCK_STREAM;

    if(( n = getaddrinfo( "127.0.0.1", TCPPORT, &hints, &res )) != 0 ){
        fprintf( stderr, "getaddrinfo: %s\n", gai_strerror( n ));
        exit( -1 );
    }

    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 400000;

	struct addrinfo *iter;
	for( iter = res; iter != NULL; iter = iter->ai_next ){
		if(( sockfd = socket( iter->ai_family, iter->ai_socktype, iter->ai_protocol )) < 0 ){
			continue;
		}
		if( setsockopt( sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof( tv )) < 0) {
            perror("Error setsockopt");
        }
        if( connect( sockfd, iter->ai_addr, iter->ai_addrlen) <0 ){
            continue;
        }
		break;
	}

    if( iter == NULL ){
        perror( "Error in connecting TCP" );
        exit( -2 );
    }
    freeaddrinfo( res );

    return sockfd;
}


/*This function handles received TCP message
    sockfd = TCP socket
    buffer = TCP buffer
    n      = TCP status
Returns 1 if everything is successful, -1 if error occured and -2 if connection was closed by server*/
int receiveChat( int sockfd, char *buffer, int n ){
    if( n < 0 ){
        perror( "Error in receiving chat message!" );
        return -1;
    } else if( n == 0 ){
        perror( "Chat connection closed by server" );
        return -2;
    } else
        fputs( buffer, stdout );

    return 1;
}


/*Handles users input and sends UDP and TCP message to server
    TCPconnection = TCP socket
    UPDconnection = UDP address information
    buffer        = data to be sent
    size          = buffer size, as it cannot be asked in functions (return standard variable size)
Returns 0 if everything worked and 1 if user wants to quit program*/
int sendMessage( int TCPconnection, UDPConnection *UDPconnection, char *buffer, size_t size ){
    int n;
    uint16_t message;
    char sendBuffer[ 1024 ];
    memset( sendBuffer, 0, sizeof( sendBuffer ));

    fgets( buffer, size, stdin );

    if( buffer[ 0 ] != '/' ){
        printf( "Invalid command - start all commands with '/'! - %c\n", buffer[0] );
        return -1;
    }

    char *action = strtok( buffer, " " );
    if( action[ strlen( action ) - 1 ] == '\n' ){                               //If input has no space and is standalone command.
        action = strtok( buffer, "\n" );
    }

    if( strcmp( action, "/join" ) == 0 ){

        switch ( userState.isInChat ){
            case 0:

                printf( "Connecting..\n" );
                return 2;
                break;

            case 1:

                printf( "You are already in chat!\n" );
                break;

            default:
                break;
        }

    } else if( strcmp( action, "/chat" ) == 0 ){
        if( userState.isInChat ){
            char messageBuffer[ 1024 ];
            strncpy( messageBuffer, userState.name, sizeof( userState.name ));
            strcat( messageBuffer, ": " );
            strcat( messageBuffer, buffer + 6 );                                //+6 is to remove /chat from beginning.
            n = send( TCPconnection, messageBuffer, strlen( messageBuffer ), 0 );
            if( n < 0 ){
                perror( "Error in sending message" );
            }
        } else{
            printf( "You must join a chat first!\n" );
        }

    } else if( strcmp( action, "/quit" ) == 0 ){

        switch ( userState.isInChat ){
            case 0:

                printf( "Quitting..\n" );
                return 1;

            case 1:

                message = htons( CHAT_QUIT );
                memcpy( sendBuffer, &message, sizeof( message ));
                n = sendto( UDPconnection->socket, sendBuffer, sizeof( sendBuffer ), 0, UDPconnection->serverAddress, UDPconnection->addrlen );
                userState.isInChat = 0;
                break;

            default:
                break;
        }

    } else if( strcmp( action, "/help" ) == 0 ){

        printHelp();

    } else if( strcmp( action, "/name" ) == 0 ){

        switch( userState.isInChat ){
            case 0:

                printf( "You must join active chat first\n" );
                break;

            case 1:

                message = htons( CHAT_NAME );
                memcpy( sendBuffer, &message, sizeof( message ));

                action = strtok( NULL, " " );
                action[ strlen( action ) - 1 ] = '\0';                          //Removes "\n" from the end.
                if( action == NULL || strcmp( action, " " ) == 0 || strcmp( action, "" ) == 0 ){
                    printf( "You must provide username\n" );
                    return 0;
                }
                memcpy( sendBuffer + sizeof( message ), action, sizeof( action ));
                n = sendto( UDPconnection->socket, sendBuffer, sizeof( sendBuffer ), 0, UDPconnection->serverAddress, UDPconnection->addrlen );
                break;

            default:
                break;
        }

    } else{
        printf( "Invalid choice!\n" );
    }

    return 0;
}


/*Handles received UDP messages
    UPDconnection = UDP address information
    buffer        = data received
*/
void receiveChatData( UDPConnection *UDPconnection, char *buffer ){
    uint16_t message;
    char msgBuffer[BUFFER_SIZE];

    memcpy( &message, buffer, sizeof( message ));
    message = ntohs( message );

	switch( message ){
		case CHAT_OK:

            printf( "UPD: OK\n" );
            memcpy( msgBuffer, buffer + sizeof( message ), sizeof( msgBuffer ) - sizeof( message ));
            printf( "Name we got: %s\n", msgBuffer );
            strncpy( userState.name, msgBuffer, UNAME_SIZE );
			break;

		case CHAT_MSG:

            printf( "UPD: START\n" );
            memcpy( msgBuffer, buffer + sizeof( message ), sizeof( msgBuffer ) - sizeof( message ));
            printf(msgBuffer,"\n");

			break;

		case CHAT_NAME:

		    printf( "UDP: NAME\n" );
		    memcpy( msgBuffer, buffer + sizeof( message ), sizeof( msgBuffer ) - sizeof( message ));
		    strncpy( userState.name, msgBuffer, UNAME_SIZE );
		    printf( "Username received: %s\n", userState.name );
			break;

		case CHAT_ERROR:

		    printf( "UPD: ERROR\n" );
		    memcpy( &message, buffer + sizeof( message ), sizeof( message ));
		    message = ntohs( message );
		    switch( message ){
		        case ERROR_NO_USER:

		            printf( "ERROR: No such user\n" );
		            break;

		        case ERROR_ALREADY_JOINED:

		     	    printf( "ERROR: Already in chat\n" );
		            break;


		        case ERROR_SERVER_SHUTDOWN:

		            printf( "ERROR: Server has shut down\n" );
		            break;

		        case ERROR_CHAT_FULL:

		            printf( "ERROR: Server is currently full\n" );
		            break;

		        default:

		            printf( "ERROR: Unknown error\nReceived: %i\n", message );
		    }

		    break;

		default:
			printf( "ERROR: Error in interpreting server reply!\nReceived: %i\n", message );
	}
}


/*This function handles the signals
    signal = signal to be handled
*/
void sighandler(int signal) {
    printf("signal\n");
    char sendBuffer[BUFFER_SIZE];
    uint16_t message;
    // Unique cases could be added, but for now they share the same functionality
    switch ( signal ){

        case SIGINT:
            printf("SIGINT\n");
            break;

        case SIGABRT: // abnormal termination

            printf("SIGABRT\n");
            break;

        case SIGBUS: // hardware fault

            printf("SIGBUG\n");
            break;

        default:
            printf("DEFAULT\n");
            break;
    }
    message = htons( CHAT_QUIT );
    memcpy( sendBuffer, &message, sizeof( message ));
    sendto( UDPconnection->socket, sendBuffer, sizeof( sendBuffer ), 0, UDPconnection->serverAddress, UDPconnection->addrlen );

    exit( -5 );
}

/*Prints the help menu for the user
*/
void printHelp(){
    printf( "Following commands are accepted:\n" );
    printf( "Format is following /command <argument>, which is written to console as /command yourArgument\n" );
    printf( "/join to join channel\n" );
    printf( "/chat <message> to chat\n" );
    printf( "/name <nickname> to change your name displayed to others\n");
    printf( "/quit to quit\n" );
    printf( "/help to see available commands\n" );
}

/*This function binds the signals to the handling function (sighandler())

*/
void initSigHandler() {
    struct sigaction signals = {
        .sa_handler = sighandler,	                        //Handling function
		.sa_flags = 0                                       //Additional flags
    };		                                    
	sigemptyset( &signals.sa_mask );					    //Create new empty signal mask

	//Add some signals
	sigaction( SIGINT,  &signals, NULL );
	sigaction( SIGABRT, &signals, NULL );
	sigaction( SIGBUS,  &signals, NULL );
}


//*************************************************************************************************************************************
int main( int argc, char **argv ){
    int TCPconnection = -1, n = 0;
    UDPconnection = NULL;
    char buffer[ BUFFER_SIZE ];

	/***************************************************************/
	//UNUSED, BUT CAN BE USEFUL IN SELECT.
	//struct timeval timeout;
    //timeout.tv_sec = 0;
    //timeout.tv_usec = 100000;                               //100 msec
    /***************************************************************/

    fd_set fdMaster, fdActive;
    FD_ZERO( &fdMaster );
    FD_ZERO( &fdActive );
    FD_SET( 0, &fdMaster );
    int fdMax = 0;

    userState.isInChat = 0;

    printHelp();

    int running = 1;
    while( running ){
        //FD_ZERO( &fdActive );
        fdActive = fdMaster;
        if( select( fdMax + 1, &fdActive, NULL, NULL, NULL ) == -1 ){           //If timeout is used, FD_ISSET( UDP ) must be changed/checked for NULL.
            perror( "Select" );
            exit( -3 );
        }
        if( FD_ISSET( TCPconnection, &fdActive )){

            n = recv( TCPconnection, buffer, sizeof( buffer ), 0 );
            buffer[ n ] = 0;
            n = receiveChat( TCPconnection, buffer, n );
            if( n == -2 ){
                exit( -2 );
            }

        } else if( FD_ISSET( 0, &fdActive )){

            n = sendMessage( TCPconnection, UDPconnection, buffer, sizeof( buffer ));
            if( n == 1 ){

                running = 0;
                close( TCPconnection );
                FD_CLR( TCPconnection, &fdMaster );
                userState.isInChat = 0;
                break;

            } else if( n == 2 ){

                UDPconnection = connectUDP( "127.0.0.1" );
                TCPconnection = connectTCP();
                initSigHandler( UDPconnection );
                FD_SET( UDPconnection->socket, &fdMaster );
                FD_SET( TCPconnection, &fdMaster );
                fdMax = TCPconnection > UDPconnection->socket ? TCPconnection : UDPconnection->socket;
                userState.isInChat = 1;
            }

        } else if( FD_ISSET( UDPconnection->socket, &fdActive )){

            n = recvfrom( UDPconnection->socket, &buffer, sizeof( buffer ), 0, NULL, NULL);
			buffer[ n ] = 0;
            receiveChatData( UDPconnection, buffer );

        }
    }
	close( TCPconnection );
	close( UDPconnection->socket );
	free( UDPconnection );
	return 0;
}
//make
