#include "server.h"

Users *users;
int UDPconnection;


/* Binds servers port to UDP or TCP.
	type = SOCK_DGRAM or SOCK_STREAM (UDP OR TCP )
	port = port to use
Returns socket descriptor of connection or UDP.*/
int bindAddress( int type, char *port ){
	int sockfd;
	struct addrinfo hints, *res;
	memset( &hints, 0, sizeof( hints ));
	
	hints.ai_family 	= AF_UNSPEC;
	hints.ai_socktype 	= type;
	hints.ai_flags 		= AI_PASSIVE;
	
	if( getaddrinfo( NULL, port, &hints, &res ) != 0 ){
		perror( "Error in getting addrinfo\n" );
		exit( -1 );
	}
	
	struct addrinfo *iter;
	for( iter = res; iter != NULL; iter = iter->ai_next ){
		if(( sockfd = socket( iter->ai_family, iter->ai_socktype, iter->ai_protocol )) < 0){
			continue;
		}
		int yes = 1;
		if (setsockopt( sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes ) == -1 ) {
    		perror("setsockopt");
    		exit(1);
		} 
		if( bind( sockfd, iter->ai_addr, iter->ai_addrlen ) < 0 ){
			close( sockfd );
			continue;
		}
		break;
	}	
	
	if( iter == NULL ){
		perror( "Error in binding" );
		exit( -2 );
	}	
	
	freeaddrinfo( res );
	return sockfd;
}


/* Checks if user is already connected to server
	*users = all connected users
	address = address information of user
Returns 1 if connected and 0 if not*/
int hasUser( Users *users, struct sockaddr_in address ){
	Users *current = users;
	while( current != NULL ){
		if( current->address.sin_port == address.sin_port && current->address.sin_addr.s_addr == address.sin_addr.s_addr )
			return 1;	
		current = current-> next;
	}
	return 0;
}


/*This function finds a single user from all users, uses combination of IP address and port number as identifier
	users = List of all users connected to server
	address = Address of a user, includes IP address and port number
Returns user if found, NULL if not*/
Users* findUser( Users *users, struct sockaddr_in address ){
	Users *current = users;
	while( current != NULL ){
		if( current->address.sin_port == address.sin_port && current->address.sin_addr.s_addr == address.sin_addr.s_addr )
			return current;	
		current = current-> next;
	}
	return NULL;
}


/*This function randomly chooses a username from a text file, that is then given to connecting user
	name = pointer to users name
*/
void generateUsername( char *name ){
	int rowCount = 0;
	char line[1024];
	FILE *fp;
	
	fp = fopen("names.txt", "r");
	if(fp == NULL) {
		perror("Error opening file");
	}
	while( fgets(line,sizeof(line),fp) != NULL){
		rowCount++;
	}
	fseek(fp, 0, SEEK_SET);
	int r = rand() % rowCount;
	
	for(int i = 0; i < r; i++){
		fgets(line, sizeof(line), fp);
	}
	fgets(line, sizeof(line), fp);
	line[ strlen(line) - 1 ] = '\0';
	strcpy(name,line);
    fclose(fp);
	printf( "Generated: %s\n", name );
	
}


//Returns created user. ID's are not unique after the added logic for user removal, but they are not used so far. Instead combination of IP address and port number identifies everyone.
Users *addUser( Users **users, struct sockaddr_in address ){
	int n = 1;
	Users *newUser;

	if( *users == NULL ){
		*users = malloc( sizeof( Users ));
		newUser = *users;
		n = 0;
	} else{
		Users *selected = *users;
		while( selected->next != NULL ){
			n += 1;
			selected = selected->next;
		}
		selected->next	= malloc( sizeof( Users ));
		newUser			= selected->next;
	}
	newUser->id			= n++;
	newUser->address	= address;
	newUser->next		= NULL;
	
	generateUsername( newUser->name );
	
	printf( "Number of users: %i\n", n );

	return newUser;
}


//Returns amount of connected users.
uint8_t getUsersAmount(){
	Users *user = users;
	uint8_t n = 0;
	while( user != NULL ){
		n++;
		user = user->next;
	}
	return n;
}


/*Removes user from active users list
	address = address of user to be removed
*/
void removeUserByAddress( struct sockaddr_in address ){
	Users *current = users->next;
	Users *previous = users;
	if( users->address.sin_port == address.sin_port && users->address.sin_addr.s_addr == address.sin_addr.s_addr ){
		users = users->next;
		free( previous );
	} else{
		while( current->address.sin_port != address.sin_port && current->address.sin_addr.s_addr != address.sin_addr.s_addr ){
			previous = current;
			current = current->next;
		}
		if( current == NULL ){
			printf( "ERROR IN REMOVING USER: NOT FOUND!\n" );
			return;
		}
		if( current->next == NULL ){
			free( current );
			previous->next = NULL;
		} else{
			previous->next = current->next;
			free( current );
		}
	}
}


/*Handles received UPD message
	socket  = UPD file descriptor
	buffer  = received message
	address = senders address information
*/
void handleUDPConnection( int socket, char *buffer, struct sockaddr_in address ){
	int n;
	uint16_t action, reply;
	char sendBuffer[ 1024 ];
	Users *user;
	memset( sendBuffer, 0, sizeof( sendBuffer ));
	
	//WAYS OF READING MESSAGE FROM BUFFER
	//action = ntohs( *(uint16_t*) buffer);
	memcpy( &action, buffer, sizeof( action ));
	action = ntohs( action );
	//***********************************
	printf( "UDP Action: %i\n", action );
	switch( action ){
		case CHAT_JOIN:
		
			printf( "UPD: JOIN\n" );
			if( getUsersAmount() < MAX_USERS ){
				if( !hasUser( users, address )){
					user = addUser( &users, address);
					reply = htons( CHAT_OK );
					memcpy( sendBuffer, &reply, sizeof( reply ));
					memcpy( sendBuffer + sizeof( reply ), user->name, sizeof( user->name ));
	    			if(( n = sendto( socket, &sendBuffer, sizeof( sendBuffer ), 0, ( struct sockaddr* )(&address), sizeof( address ))) < 0 )
						perror( "Error in sending UDP" );		
				} else{											//If player has already joined.
				reply = htons( CHAT_ERROR );
				memcpy( sendBuffer, &reply, sizeof( reply ));
				reply = htons( ERROR_ALREADY_JOINED );
				memcpy( sendBuffer + sizeof( reply ), &reply, sizeof( reply ));
				n = sendto( socket, &sendBuffer, sizeof( sendBuffer ), 0, ( struct sockaddr* )(&address), sizeof( address ));
				}
			} else{
				reply = htons( CHAT_ERROR );
				memcpy( sendBuffer, &reply, sizeof( reply ));
				reply = htons( ERROR_CHAT_FULL );
				memcpy( sendBuffer + sizeof( reply ), &reply, sizeof( reply ));
				n = sendto( socket, &sendBuffer, sizeof( sendBuffer ), 0, ( struct sockaddr* )(&address), sizeof( address ));				
			}
			break;
			
			
		case CHAT_QUIT:
		
			printf( "UPD: QUIT\n" );
			removeUserByAddress( address );
			
			//Notify everyone		- NOT IMPLEMENTED.
			break;
			
		case CHAT_NAME:
			
			printf( "UPD: NAME\n" );
			user = findUser( users, address );
			memcpy( user->name, buffer + sizeof( action ), UNAME_SIZE );
			printf( "Sent name: %s\n", user->name );
			reply = htons( CHAT_NAME );
			memcpy( sendBuffer, &reply, sizeof( reply ));
			memcpy( sendBuffer + sizeof( reply ), user->name, sizeof( user->name ));
			n = sendto( socket, &sendBuffer, sizeof( sendBuffer ), 0, ( struct sockaddr* )(&address), sizeof( address ));			
			break;
			
		case CHAT_ERROR:
		
			printf( "UDP: ERROR\n" );

			break;
			
		default:
			printf( "Error in interpreting client command!\nReceived: %i\n", action );
	}
}


/*This function handles the signals
    signal = signal to be handled
*/
void sighandler(int signal) {
    printf("signal\n");
    char sendBuffer[BUFFER_SIZE];
    uint16_t message;
    // Unique cases could be added, but for now they share the same functionality
    switch ( signal ){
        
        case SIGINT:
            printf("SIGINT\n");
            break;
            
        case SIGABRT: // abnormal termination
            
            printf("SIGABRT\n");
            break;
            
        case SIGBUS: // hardware fault
            
            printf("SIGBUG\n");
            break;
        
        default:
            printf("DEFAULT\n");
            break;
    }
	// Removes users from the server and closes it
    Users *user = users;
	while( user != NULL ){
		message = htons( CHAT_ERROR );
		memcpy( sendBuffer, &message, sizeof( message ));
		message = htons( ERROR_SERVER_SHUTDOWN );
		memcpy( sendBuffer + sizeof( message ), &message, sizeof( message ));
		sendto( UDPconnection, sendBuffer, sizeof( sendBuffer ), 0, ( struct sockaddr* )&user->address, sizeof( user->address ));

		user = user->next;
	}
    exit( -5 );
}


/*This function binds the signals to the handling function (sighandler())

*/
void initSigHandler() {
    struct sigaction signals = { 	.sa_handler = sighandler,	                //Handling function
					.sa_flags = 0 };		                                    //Additional flags
	sigemptyset(&signals.sa_mask);					                            //Create new empty signal mask
		
	//Add some signals
	sigaction(SIGINT, &signals,NULL);
	sigaction(SIGABRT,&signals,NULL);
	sigaction(SIGBUS,&signals,NULL);
}


//*************************************************************************************************************************************
int main( int argc, char *argv[] ) {
	int n, newConnection;
    struct sockaddr_in clientAddress;
    socklen_t addrlen = sizeof( clientAddress );
    
	char buffer[ BUFFER_SIZE ];
	memset( buffer, 0, sizeof( buffer ));
	
	UDPconnection = bindAddress( SOCK_DGRAM, UDPPORT );
	int TCPconnection = bindAddress( SOCK_STREAM, TCPPORT );
	
	if( listen( TCPconnection, QUEUE_SIZE ) == -1 ){
		perror( "Error in listening" );
		exit( -3 );
	}	
	
	
	fd_set fdMaster, fdActive;
	int fdMax = TCPconnection > UDPconnection ? TCPconnection : UDPconnection;
	FD_ZERO( &fdMaster );
	FD_ZERO( &fdActive );
	FD_SET( UDPconnection, &fdMaster );
	FD_SET( TCPconnection, &fdMaster );
	
	users = NULL;
	
	initSigHandler();
	
	while( 1 ){
		fdActive = fdMaster;
		if( select( fdMax + 1, &fdActive, NULL, NULL, NULL ) == -1 ){
			perror( "Error in select" );
			exit( - 4 );
		}
		
		for( int i = 0; i <= fdMax; i++ ){
			if( FD_ISSET( i, &fdActive )){
				
				//Add new connection.
				if( i == TCPconnection ){

					newConnection = accept(  TCPconnection, NULL, NULL ); 
					if( newConnection == -1 ){
						perror( "Error in accept" );
						
					} else{
						FD_SET( newConnection, &fdMaster );
						fdMax = fdMax < newConnection ? newConnection : fdMax;
						printf( "New TCP connection\n" );
						send( newConnection, "Welcome to chat!\n", sizeof( "Welcome to chat!\n" ), 0 );
					}
				} else if( i == UDPconnection ){
					
					n = recvfrom( UDPconnection, &buffer, sizeof( buffer ), 0, (struct sockaddr*)( &clientAddress ), &addrlen );
					buffer[ n ] = 0;
					handleUDPConnection( UDPconnection, buffer, clientAddress );		//Fun bug: select without timeout is faster than function call leading here twice on single message.

				} else {
					//TCP data from existing client.
					n = recv( i, buffer, sizeof( buffer ), 0 );
					buffer[ n ] = 0;
					//We get here if error occurs, or client closes connection.
					printf( "Chat messagelen: %i\n", n );
					printf( "Message: %s\n", buffer );
					if( n <= 0 ){
						if( n == 0 )
							printf( "TCP connection closed\n" );
						else
							perror( "Error in recv" );
						close( i );
						FD_CLR( i, &fdMaster );
					} else{
						//Getting data from client -> send to everyone except listener (original sender) and server.
						for( int j = 0; j <= fdMax; j++ ){
							if( FD_ISSET( j, &fdMaster )){
								if( j != TCPconnection && j != i && j != UDPconnection ){
									if( send( j, buffer, n, 0 ) == -1 ){
										perror( "Error in sending" );
									}
								}
							}
						} 
					}
				}
			}
		}
	}
	close( TCPconnection );
	close( UDPconnection );
	return 0;
}
//gcc server.c -o server -std=c11 -Wall