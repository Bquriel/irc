#define _POSIX_C_SOURCE 200112L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>

#define TCPPORT "9001"
#define UDPPORT "9002"
#define	BUFFER_SIZE 1024
#define QUEUE_SIZE 15
#define MAX_USERS 100
#define UNAME_SIZE 15


struct users{
	int id;
	char name[ UNAME_SIZE ];
	struct sockaddr_in address;
	struct users *next;
}; typedef struct users Users;


enum messageType{
    CHAT_JOIN,
    CHAT_OK,
    CHAT_MSG,
    CHAT_NAME,
    CHAT_QUIT,
    CHAT_ERROR
}; typedef enum messageType MessageType;

enum errorType{
    ERROR_NO_USER,
    ERROR_CHAT_FULL,
    ERROR_ALREADY_JOINED,
    ERROR_SERVER_SHUTDOWN
}; typedef enum errorType ErrorType;

int bindAddress( int type, char *port );
int hasUser( Users *users, struct sockaddr_in address );
Users* findUser( Users *users, struct sockaddr_in address );
void generateUsername( char *name );
Users *addUser( Users **users, struct sockaddr_in address );
uint8_t getUsersAmount();
void removeUserByAddress( struct sockaddr_in address );
void handleUDPConnection( int socket, char *buffer, struct sockaddr_in address );
void sighandler(int signal);
void initSigHandler();